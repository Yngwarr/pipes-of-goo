#!/bin/bash

#set -x

BUILD_DIR='_build'
INDEX="$BUILD_DIR/index.html"

MODE="$1"
PROD_FLAGS=''

if [[ "$MODE" != 'dev' ]] ; then
    PROD_FLAGS='--minify'
fi

rm -rf "$BUILD_DIR"
mkdir "$BUILD_DIR"

for f in * ; do
    if [[ "${f::1}" == '_' ]] ; then continue ; fi
    if [[ "$f" == 'build.sh' ]] ; then continue ; fi
    if [[ "$f" == 'js' ]] ; then continue ; fi

    echo "Copying $f..."
    cp -r "$f" "$BUILD_DIR"
done

echo "Building app.js..."

esbuild js/main.js --bundle $PROD_FLAGS --sourcemap --target=chrome58,firefox57,safari11,edge16 --outfile="$BUILD_DIR/app.js"

echo "Patching index.html..."
awk -f '_tools/cut-html.awk' 'index.html' > "$INDEX"

echo "Done!"
