import Pipe, {Dir, opposite} from "./Pipe";
import { Event } from "./Event";
import Barrel from "./Barrel";
import Tools from "./Tools";
import Boom from "./Boom";
import GooFountain from "./GooFountain";

function gridTo(gx, gy, dir) {
    switch (dir) {
        case Dir.up: return [gx, gy - 1];
        case Dir.down: return [gx, gy + 1];
        case Dir.left: return [gx - 1, gy];
        case Dir.right: return [gx + 1, gy];
    }
}

export default class Grid extends PIXI.Container {
    constructor(w, h) {
        super();

        this.filtered = false;

        this.w = w;
        this.h = h;
        this.pipes = [];

        this.preview = new Pipe(Dir.none);
        this.preview.alpha = .5;
        this.preview.visible = false;

        this.preventClicking = false;

        this.interactive = true;
        this.on('pointerover', () => this.preview.visible = true);
        this.on('pointerout', () => this.preview.visible = false);

        game.stage.on(Event.nextChanged, dir => this.preview.dir = dir);
        game.stage.on(Event.flowComplete, args => {
            const coords = gridTo(args.gx, args.gy, args.dir);
            const p = this.at(...coords);

            if (p === null) {
                game.stage.emit(Event.flowFailed, {
                    gx: coords[0],
                    gy: coords[1],
                    from: opposite(args.dir)
                });
                return;
            }

            p.flow(coords[0], coords[1], opposite(args.dir));
        });
        game.stage.on(Event.flowFailed, args => {
            this.preventClicking = true;

            if (!this.filtered) {
                this.splash.play(args.gx, args.gy, args.from);
            }

            const toBreak = this.pipes.filter(x => x.empty && x.dir !== Dir.none);
            const breakPipes = () => {
                if (toBreak.length === 0) {
                    game.stage.emit(Event.explosionsEnd);
                    return;
                }
                game.stage.emit(Event.pipeBreak);
                const p = toBreak.shift();
                p.dir = Dir.none;
                this.boom.explode(p.position.x, p.position.y, breakPipes);
            };
            breakPipes();
        });
        game.stage.on(Event.filtered, () => this.filtered = true);

        for (let i = 0; i < w * h; ++i) {
            const p = new Pipe(Dir.none, this);

            p.interactive = true;
            p.on('pointerover', () => {
                this.preview.position.set(p.position.x, p.position.y)
            });
            p.on('pointerdown', () => {
                if (this.preventClicking) return;
                if (!p.empty) return;

                if (p.dir !== Dir.none) {
                    const dir = this.preview.dir;
                    p.dir = Dir.none;
                    this.preventClicking = true;
                    game.stage.emit(Event.pipeExploded);
                    this.boom.explode(p.position.x, p.position.y, () => {
                        p.dir = dir;
                        this.preventClicking = false;
                    }, .75);
                } else {
                    p.dir = this.preview.dir;
                    PIXI.sound.play('snd_pipe');
                }
                game.stage.emit(Event.pipePlaced);
            });

            this.pipes.push(p);
            this.addChild(p);
        }

        this.boom = new Boom();
        this.addChild(this.boom);

        this.splash = new GooFountain();
        this.addChild(this.splash);

        this.forEach((p, x, y) => {
            const shade = PIXI.Sprite.from('grid_shade');
            shade.anchor.set(.5);
            shade.position.set(x * Pipe.size, y * Pipe.size);
            this.addChildAt(shade, 0);

            p.position.set(x * Pipe.size, y * Pipe.size);
        });

        this.addChild(this.preview);
    }

    dropBarrel() {
        const x = Math.random() * this.w | 0;
        const y = Math.random() * this.h | 0;
        // TODO make sure this doesn't pick an impossible position
        const availDirs = this.dirs(x, y);

        this.barrel = new Barrel(x, y, Tools.sample(availDirs));
        this.barrel.position.set(x * Pipe.size, y * Pipe.size);
        this.at(x, y).interactive = false;
        this.addChild(this.barrel);

        this.barrel.open();
    }

    at(x, y) {
        if (x < 0 || x >= this.w || y < 0 || y >= this.h) {
            return null;
        }
        return this.pipes[y * this.w + x];
    }

    dirs(x, y) {
        const dirs = [Dir.left, Dir.right, Dir.up, Dir.down];
        const r = [];

        for (const d of dirs) {
            const p = this.at(...gridTo(x, y, d));
            if (p === null) continue;
            if (!p.interactive) continue;
            r.push(d);
        }

        return r;
    }

    forEach(f) {
        for (let y = 0; y < this.h; ++y) {
            for (let x = 0; x < this.w; ++x) {
                f(this.at(x, y), x, y);
            }
        }
    }

    toString() {
        let s = '';
        for (let y = 0; y < this.h; ++y) {
            for (let x = 0; x < this.w; ++x) {
                s += this.at(x, y);
            }
            s += '\n';
        }
        return s;
    }

    update() {
        for (const p of this.pipes) {
            p.update();
        }
    }
}
