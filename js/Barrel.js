import {Dir} from "./Pipe";
import Tools from "./Tools";
import {Event} from "./Event";

export default class Barrel extends PIXI.Sprite {
    constructor(gx, gy, dir) {
        super(PIXI.Texture.from("barrel (bg) 0.aseprite"));

        this.gx = gx;
        this.gy = gy;
        this.anchor.set(.5);

        let doorName = '';
        switch (dir) {
            case Dir.left: doorName = 'left'; break;
            case Dir.right: doorName = 'right'; break;
            case Dir.up: doorName = 'up'; break;
            case Dir.down: doorName = 'down'; break;
        }

        this.door = Tools.spriteAnim(
            Tools.range(6).map(x => `barrel (door_${doorName}) ${x}.aseprite`)
        );
        this.door.anchor.set(.5);
        this.door.animationSpeed = .01;
        this.door.loop = false;
        //debugger;
        const overlay = new PIXI.filters.ColorOverlayFilter(0xff0000);
        this.door.filters = [overlay];

        this.blink = PIXI.tweenManager.createTween(overlay);
        this.blink.from({ alpha: 1 }).to({ alpha: 0 });
        this.blink.time = 1000;
        this.blink.loop = true;
        this.blink.pingPong = true;
        this.blink.easing = PIXI.tween.Easing.inQuad();

        this.door.onComplete = () => {
            this.blink.stop();
            overlay.enabled = false;
            PIXI.sound.play('snd_splash');
            game.stage.emit(Event.flowComplete, { gx, gy, dir });
        }

        this.addChild(this.door);

        for (const d of ['left', 'right', 'up', 'down']) {
            if (d === doorName) continue;
            const s = PIXI.Sprite.from(`barrel (door_${d}) 0.aseprite`);
            s.anchor.set(.5);
            this.addChild(s);
        }
    }

    open() {
        this.blink.start();
        this.door.play();
    }
}
