import Tools from "./Tools";
import Dirt from "./Dirt";

export default class EndScene extends PIXI.Container {
    constructor(win, score, theme = gameDefaults.theme) {
        super();

        const dirt = new Dirt();
        this.addChild(dirt);

        const fin = Tools.spriteAnim(win
            ? Tools.range(4).concat(Tools.range(4).reverse()).map(x => `win ${x}.aseprite`)
            : Tools.range(6).map(x => `lost ${x}.aseprite`));

        fin.anchor.set(.5);
        fin.scale.set(1.5);
        fin.position.set(540, win ? 384 : 344);

        if (!win) fin.loop = false;
        fin.animationSpeed = .1;
        this.addChild(fin);

        const label = new PIXI.Text(`Final score: ${score} points`, theme.text.acid);
        if (win) label.style.fill = '#5fcde4';
        label.anchor.set(.5)
        label.position.set(540, 600);
        this.addChild(label);

        fin.interactive = true;
        fin.buttonMode = true;
        fin.on('pointerup', () => location.reload());


        fin.play();
    }
}
