import Tools from "./Tools";

const animSpeed = .2;

export default class Boom extends PIXI.Container {
    constructor() {
        super();

        this._cb = null;
        this.anim = Tools.spriteAnim(Tools.range(6).map(x => `boom ${x}.aseprite`));
        this.anim.anchor.set(.5);
        this.anim.position.set(0);
        this.anim.animationSpeed = animSpeed;
        this.anim.loop = false;
        this.visible = false;
        this.anim.onComplete = () => {
            this.visible = false;
            if (this._cb !== null) {
                this._cb();
                //this._cb = null;
            }
        }
        this.addChild(this.anim);
    }

    explode(x, y, cb = null, speed = 1) {
        this._cb = cb;
        this.anim.animationSpeed = animSpeed * speed;
        this.position.set(x, y);
        this.visible = true;
        this.anim.gotoAndPlay(0);
        PIXI.sound.play('snd_boom');
    }
}
