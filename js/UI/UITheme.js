export default class UITheme {
    constructor(config = {}) {
        const conf = Object.assign({
            btnIdle: "grey_button15.png",
            btnHover: "grey_button01.png",
            btnPressed: "grey_button02.png",
            btnTextStyle: {
                fontFamily: 'IBM',
                fontSize: 16
            },
            labelTextStyle: {
                fontFamily: 'IBM',
                fontSize: 24,
                fill: 'white',
                strokeThickness: 2
            },
            acidTextStyle: {
                fontFamily: 'IBM',
                fontSize: 24,
                fill: '#6abe30',
                strokeThickness: 2
            },
        }, config);

        this.textures = {
            btnIdle: PIXI.Texture.from(conf.btnIdle),
            btnHover: PIXI.Texture.from(conf.btnHover),
            btnPressed: PIXI.Texture.from(conf.btnPressed)
        };

        this.text = {
            btn: conf.btnTextStyle,
            label: conf.labelTextStyle,
            acid: conf.acidTextStyle
        };
    }
}
