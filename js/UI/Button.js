const TEXT_OFFSET = -3;

export default class Button extends PIXI.Sprite {
    constructor(textLine, callback, theme = gameDefaults.theme) {
        super(theme.textures.btnIdle);

        this.theme = theme;
        this.anchor.set(.5);
        this.interactive = true;
        this.buttonMode = true;

        this.callback = callback;
        this.text = new PIXI.Text(textLine, theme.text.btn);
        this.text.anchor.set(.5);
        this.text.position.set(0, TEXT_OFFSET);
        this.addChild(this.text);

        this.on('pointerdown', this.onDown)
            .on('pointerup', this.onUp)
            .on('pointerupoutside', this.onUpOutside)
            .on('pointerover', this.onOver)
            .on('pointerout', this.onOut);

        this.pressed = false;
    }

    onDown() {
        this.texture = this.theme.textures.btnPressed;
        this.text.position.y = 0;
        this.pressed = true;
    }

    onUp() {
        this.texture = this.theme.textures.btnHover;
        this.text.position.y = TEXT_OFFSET;
        this.pressed = false;
        this.callback();
    }

    onUpOutside() {
        this.texture = this.theme.textures.btnIdle;
        this.text.position.y = TEXT_OFFSET;
        this.pressed = false;
    }

    onOver() {
        if (this.pressed) return;
        this.texture = this.theme.textures.btnHover;
    }

    onOut() {
        if (this.pressed) return;
        this.texture = this.theme.textures.btnIdle;
    }
}
