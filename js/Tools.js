export const ResizeModes = {
    ignoreRatio: 0,
    keepW: 1,
    keepH: 2
};

export default class Tools {
    static resize(el, mode) {
        switch (mode) {
            case ResizeModes.ignoreRatio:
                return size => el.scale.set(
                    size.w / el.texture.width,
                    size.h / el.texture.height
                );
            case ResizeModes.keepW:
                return size => el.scale.set(
                    size.w / el.texture.width
                );
            case ResizeModes.keepH:
                return size => el.scale.set(
                    size.h / el.texture.height
                );
        }
    }

    static shuffle(xs) {
        for (let i = xs.length - 1; i > 1; --i) {
            const j = (Math.random() * (i + 1)) | 0;
            const t = xs[j];
            xs[j] = xs[i];
            xs[i] = t;
        }
    }

    static sample(xs) {
        return xs[(Math.random() * xs.length)|0];
    }

    static spriteAnim(textureNames) {
        const ts = [];
        for (const name of textureNames) {
            ts.push(PIXI.Texture.from(name));
        }
        return new PIXI.AnimatedSprite(ts);
    }

    static range(start, end = null, step = 1) {
        const r = [];
        for (let i = (end === null ? 0 : start); i < (end === null ? start : end); i += step) {
            r.push(i);
        }
        return r;
    }
}
