import Grid from "./Grid";
import PipeQueue from "./PipeQueue";
import Dirt from "./Dirt";
import { Event } from "./Event";
import EndScene from "./EndScene";

export default class GameScene extends PIXI.Container {
    constructor(theme = gameDefaults.theme) {
        super();

        this.clk = 0;
        this.updateTick = 3;

        this.toFilter = 18;
        //this.toFilter = 3;
        game.stage.on(Event.flowComplete, args => {
            this.addScore((this.toFilter === 0 ? 2 : 1) * (args.cross ? 500 : 50));

            if (this.toFilter === 0) return;

            this.toFilter--;
            if (this.toFilter === 0) {
                game.stage.emit(Event.filtered);
                this.filterLabel.style.fill = '#5fcde4';
                this.filterLabel.text = 'Filtered!';
            } else {
                this.filterLabel.text = `${this.toFilter} left`;
            }
        });
        game.stage.on(Event.flowFailed, args => {
            PIXI.sound.stop('music');
        });
        game.stage.on(Event.explosionsEnd, args => {
            setTimeout(() => {
                const endScene = new EndScene(this.toFilter <= 0, this.score);
                this.addChild(endScene);
            }, 1000)
        });
        game.stage.on(Event.pipeExploded, () => this.addScore(-50));
        game.stage.on(Event.pipeBreak, () => this.addScore(-100));

        this.dirt = new Dirt();

        this.grid = new Grid(10, 7);
        this.grid.position.set(300, 200);

        this.grid.dropBarrel();

        this.queue = new PipeQueue();
        this.queue.position.set(150, 200);

        this.addChild(this.dirt);
        this.addChild(this.grid);
        this.addChild(this.queue);

        this.score = 0;
        this.scoreLabel = new PIXI.Text(`Score: ${this.score}`, theme.text.label);
        this.scoreLabel.position.set(12);
        this.addChild(this.scoreLabel);

        this.filterLabel = new PIXI.Text(`${this.toFilter} left`, theme.text.acid);
        this.filterLabel.anchor.set(1, 0);
        this.filterLabel.position.set(1068, 12);
        this.addChild(this.filterLabel);

        PIXI.sound.play('music');
    }

    addScore(value) {
        if (this.score < -value) {
            this.score = 0;
        } else {
            this.score += value;
        }

        this.scoreLabel.text = `Score: ${this.score}`;
    }

    // 1 delta = 1 / 60 second
    update(delta) {
        this.clk += delta;

        if (this.clk < this.updateTick) return;

        this.clk -= this.updateTick;
        this.grid.update();
    }
}
