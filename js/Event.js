export const Event = {
    resized: 'resized',
    nextChanged: 'nextChanged',
    pipePlaced: 'pipePlaced',
    flowComplete: 'flowComplete',
    flowFailed: 'flowFailed',
    filtered: 'filtered',
    pipeExploded: 'pipeExploded',
    pipeBreak: 'pipeBreak',
    explosionsEnd: 'explosionsEnd'
};
