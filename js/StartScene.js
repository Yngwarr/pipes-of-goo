import Tools from "./Tools";
import Dirt from "./Dirt";

export default class StartScene extends PIXI.Container {
    constructor() {
        super();

        const dirt = new Dirt();
        this.addChild(dirt);

        const start = Tools.spriteAnim(Tools.range(4).map(x => `start ${x}.aseprite`));
        start.anchor.set(.5);
        start.scale.set(2);
        start.position.set(540, 384);

        start.animationSpeed = .1;
        start.loop = false;

        start.interactive = true;
        start.buttonMode = true;
        start.on('pointerup', () => window.startGame());
        this.addChild(start);

        start.play();
    }
}
