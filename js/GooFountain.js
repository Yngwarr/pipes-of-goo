import Tools from "./Tools";
import Pipe, {Dir} from "./Pipe";

export default class GooFountain extends PIXI.Container {
    constructor() {
        super();

        this.anim = Tools.spriteAnim(Tools.range(7).map(x => `goo ${x}.aseprite`));
        this.anim.anchor.set(.5);
        this.anim.position.set(0);
        this.anim.animationSpeed = .2;
        this.visible = false;
        this.addChild(this.anim);
    }

    play(gx, gy, dir) {
        let x, y;
        this.visible = true;
        switch (dir) {
            case Dir.up:
                x = gx * Pipe.size;
                y = (gy - .5) * Pipe.size;
                this.angle = 180;
                break;
            case Dir.down:
                x = gx * Pipe.size;
                y = (gy + .5) * Pipe.size;
                this.angle = 0;
                break;
            case Dir.left:
                x = (gx - .5) * Pipe.size;
                y = gy * Pipe.size;
                this.angle = 90;
                break;
            case Dir.right:
                x = (gx + .5) * Pipe.size;
                y = gy * Pipe.size;
                this.angle = 270;
                break;
        }
        this.position.set(x, y);
        this.anim.play();
    }
}
