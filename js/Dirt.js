const TileSize = 64;

export default class Dirt extends PIXI.Container{
    constructor() {
        super();
        this.tilemap = new PIXI.tilemap.CompositeTilemap();
        this.addChild(this.tilemap);

        this.setupTilemap();
    }

    setupTilemap() {
        this.tilemap.clear();
        const pxW = game.renderer.screen.width;
        const pxH = game.renderer.screen.height;
        const tileW = pxW / TileSize;
        const tileH = pxH / TileSize;

        for (let x = 0; x < tileW; ++x) {
            for (let y = 0; y < tileH; ++y) {
                this.tilemap.tile(this.rollTile(x, y), x * TileSize, y * TileSize);
            }
        }
    }

    rollTile(x, y) {
        if (y === 0) return 'surface 1';
        if (y === 1) return 'surface 0';

        const roll = Math.random();
        if (roll > .2) return 'dirt 0';
        if (roll > .15) return 'dirt 1';
        if (roll > .1) return 'dirt 2';
        if (roll > .05) return 'dirt 3';
        return 'dirt 4';
    }
}
