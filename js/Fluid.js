import Pipe, { Dir } from "./Pipe";
import { Event } from "./Event";

const acid = 0x6abe30;
const water = 0x5fcde4;

export default class Fluid extends PIXI.Container {
    constructor() {
        super();

        this.cross = false;
        this.flowing = false;
        this.turningPhase = -1;

        this.first = PIXI.Sprite.from('liquid');
        this.first.tint = acid;
        this.first.anchor.set(.5, 0);
        this.first.visible = false;
        this.addChild(this.first);

        this.second = PIXI.Sprite.from('liquid');
        this.second.tint = acid;
        this.second.anchor.set(.5, 0);
        this.second.visible = false;
        this.addChild(this.second);

        const tiles = [];
        for (let i = 1; i < 25; ++i) {
            tiles.push(PIXI.Texture.from(`turn${i}`));
        }
        this.turn = new PIXI.AnimatedSprite(tiles);
        this.turn.tint = acid;
        this.turn.anchor.set(.5);
        this.turn.loop = false;
        this.turn.onComplete = () => this.turningPhase++;
        this.turn.visible = false;
        this.addChild(this.turn);

        this.filtered = false;
        game.stage.on(Event.filtered, () => this.filtered = true);
    }

    flow(gx, gy, from, to) {
        this.gx = gx;
        this.gy = gy;
        this.from = from;
        this.to = to;

        if (this.first.scale.y !== 1) {
            const tmp = this.first;
            this.first = this.second;
            this.second = tmp;
            this.cross = true;
        }

        if (from === Dir.down && to === Dir.up) {
            this.first.angle = 180;
            this.first.position.set(0, Pipe.size / 2);
            this.first.visible = true;
        } else if (from === Dir.up && to === Dir.down) {
            this.first.angle = 0;
            this.first.position.set(0, -Pipe.size / 2);
            this.first.visible = true;
        } else if (from === Dir.right && to === Dir.left) {
            this.first.angle = 90;
            this.first.position.set(Pipe.size / 2, 0);
            this.first.visible = true;
        } else if (from === Dir.left && to === Dir.right) {
            this.first.angle = 270;
            this.first.position.set(-Pipe.size / 2, 0);
            this.first.visible = true;
        } else if (
            from === Dir.right && to === Dir.up
            || from === Dir.up && to === Dir.left
            || from === Dir.left && to === Dir.down
            || from === Dir.down && to === Dir.right
        ) {
            this.first.angle = 90;
            this.first.position.set(Pipe.size / 2, 0);
            this.first.visible = true;

            this.second.angle = 180;
            this.second.position.set(0, -12);

            this.turningPhase = 0;
        } else {
            this.first.angle = 0;
            this.first.position.set(0, -Pipe.size / 2);
            this.first.visible = true;

            this.second.angle = 270;
            this.second.position.set(12, 0);

            this.turn.angle = 90;
            this.turn.scale.x = -1;

            this.turningPhase = 0;
        }

        this.flowing = true;
        if (this.filtered) {
            this.first.tint = water;
            if (this.second.scale.y === 1) this.second.tint = water;
            this.turn.tint = water;
        }
    }

    update() {
        if (!this.flowing) return;

        if (this.turningPhase === 0) {
            if (this.first.scale.y === 20) {
                this.turningPhase++;
                this.turn.visible = true;
                this.turn.play();
                return;
            }
            this.first.scale.y++;
            return;
        }

        if (this.turningPhase === 2) {
            if (this.second.scale.y === 20) {
                this.flowing = false;
                this.complete();
                return;
            }
            this.second.visible = true;
            this.second.scale.y++;
            return;
        }

        if (this.turningPhase === -1) {
            if (this.first.scale.y === Pipe.size) {
                this.flowing = false;
                this.complete();
                return;
            }
            this.first.scale.y++;
        }
    }

    complete() {
        game.stage.emit(Event.flowComplete, {
            gx: this.gx,
            gy: this.gy,
            dir: this.to,
            cross: this.cross
        });
    }
}
