import Fluid from "./Fluid";
import { Event } from "./Event";

export const Dir = {
    none: 0,
    up: 1,
    down: 2,
    left: 4,
    right: 8,
    every: 15
};

export function opposite(dir) {
    switch (dir) {
        case Dir.up: return Dir.down;
        case Dir.down: return Dir.up;
        case Dir.left: return Dir.right;
        case Dir.right: return Dir.left;
    }
}

export default class Pipe extends PIXI.Sprite {
    constructor(dir, grid = null) {
        super();
        this.anchor.set(.5);
        this.dir = dir;
        this.empty = true;

        // grid == null means Pipe is not supposed to be interactive
        this.grid = grid;

        if (this.grid !== null) {
            this.fluid = new Fluid();
            this.addChild(this.fluid);
        }
    }

    update() {
        this.fluid.update();
    }

    set dir(value) {
        this._dir = value;

        const tn = this.textureName();
        this.texture = PIXI.Texture.from(tn.name);
        this.angle = tn.angle;
    }

    get dir() {
        return this._dir;
    }

    canGo(...dirs) {
        return dirs.reduce((a, b) => a && !! (this.dir & b), true);
    }

    textureName() {
        if (this.dir === Dir.every) return { name: 'udlr', angle: 0 };
        if (this.canGo(Dir.left, Dir.right)) return { name: 'lr', angle: 0 };
        if (this.canGo(Dir.up, Dir.down)) return { name: 'ud', angle: 0 };
        if (this.canGo(Dir.down, Dir.right)) return { name: 'ur', angle: 90 };
        if (this.canGo(Dir.down, Dir.left)) return { name: 'ur', angle: 180 };
        if (this.canGo(Dir.up, Dir.right)) return { name: 'ur', angle: 0 };
        if (this.canGo(Dir.up, Dir.left)) return { name: 'ur', angle: 270 };
        if (this.dir === Dir.none) return { name: 'empty', angle: 0 };
    }

    flow(gx, gy, from) {
        if (!this.canGo(from)) {
            this.failFlowing(gx, gy, from);
            return;
        }

        this.empty = false;
        this.fluid.flow(gx, gy, from, this.dir === Dir.every
            ? opposite(from)
            : this.dir ^ from);
    }

    failFlowing(gx, gy, from) {
        game.stage.emit(Event.flowFailed, { gx, gy, from });
    }

    toString() {
        if (this.dir === Dir.every) return '┼';
        if (this.canGo(Dir.left, Dir.right)) return '─';
        if (this.canGo(Dir.up, Dir.down)) return '│';
        if (this.canGo(Dir.down, Dir.right)) return '┌';
        if (this.canGo(Dir.down, Dir.left)) return '┐';
        if (this.canGo(Dir.up, Dir.right)) return '└';
        if (this.canGo(Dir.up, Dir.left)) return '┘';
        if (this.dir === Dir.none) return ' ';
    }

    static valid() {
        return [
            Dir.every,
            Dir.left | Dir.right,
            Dir.up | Dir.down,
            Dir.down | Dir.right,
            Dir.down | Dir.left,
            Dir.up | Dir.right,
            Dir.up | Dir.left
        ];
    }
}

Pipe.size = 64;
