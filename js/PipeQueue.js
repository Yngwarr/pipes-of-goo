import Pipe from "./Pipe";
import Tools from "./Tools";
import { Event } from "./Event";

const VISIBLE = 5;

export default class PipeQueue extends PIXI.Container {
    constructor() {
        super();

        this.pool = Pipe.valid();
        this.q = [];
        this.roll();

        for (let i = 0; i < VISIBLE; ++i) {
            const p = new Pipe(this.q[i]);
            p.position.set(0, (VISIBLE - i - 1) * Pipe.size);
            this.addChild(p)
        }

        this.changeNext();

        game.stage.on(Event.pipePlaced, () => this.next());
    }

    roll() {
        const newQ = JSON.parse(JSON.stringify(this.pool));
        Tools.shuffle(newQ);
        this.q = this.q.concat(newQ);
    }

    next() {
        this.q.shift();

        if (this.q.length < VISIBLE) {
            this.roll();
        }

        for (let i = 0; i < VISIBLE; ++i) {
            this.children[i].dir = this.q[i];
        }

        this.changeNext();
    }

    changeNext() {
        game.stage.emit(Event.nextChanged, this.q[0]);
    }
}
