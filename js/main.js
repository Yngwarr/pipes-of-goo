import { Event } from "./Event";
import UITheme from "./UI/UITheme";
import GameScene from "./GameScene";
import StartScene from "./StartScene";

window.init = function () {
    const wrapper = document.getElementById('wrapper');

    window.game = new PIXI.Application({
        //resizeTo: wrapper
        width: 1080,
        height: 768
    });
    wrapper.appendChild(game.view);
    game.stop();

    window.addEventListener('resize', () => {
        game.stage.emit(Event.resized, { w: window.innerWidth, h: window.innerHeight });
    });

    loadAssets();
}

window.gameDefaults = {};

function loadAssets() {
    WebFont.load({ custom: { families: ['IBM'] } });

    game.loader
        .add('grey_ui', 'img/ui/greySheet.json')
        .add('pipes', 'img/pipes.json')
        .add('dirt', 'img/dirt.json')
        .add('empty', 'img/empty.png')
        .add('liquid', 'img/liquid.png')
        .add('fill', 'img/fill.json')
        .add('barrel', 'img/barrel.json')
        .add('grid_shade', 'img/grid-shade.png')
        .add('boom', 'img/boom.json')
        .add('goo', 'img/goo.json')
        .add('start', 'img/start.json')
        .add('win', 'img/win.json')
        .add('lost', 'img/lost.json')

        .add('snd_pipe', 'sound/pipe.ogg')
        .add('snd_boom', 'sound/boom.ogg')
        .add('snd_splash', 'sound/splash.ogg')
        .add('music', 'sound/in-the-city.ogg')
        .load(startup);
}

function startup(_loader, res) {
    res.snd_pipe.sound.volume = .4;
    res.snd_boom.sound.volume = .4;
    res.snd_splash.sound.volume = .4;
    res.music.sound.volume = .2;
    res.music.sound.loop = true;

    gameDefaults.theme = new UITheme();

    for (const e in Event) {
        game.stage.on(e, a => console.log("Event:", e, a));
    }

    const startScene = new StartScene();
    game.stage.addChild(startScene);

    let gameScene = null;
    window.startGame = () => {
        gameScene = new GameScene();
        game.stage.addChild(gameScene);
    };

    game.ticker.add(delta => {
        PIXI.tweenManager.update();
        //console.log(delta);

        if (gameScene === null) return;
        gameScene.update(delta);
    });

    game.start();
}
