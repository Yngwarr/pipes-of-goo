#!/bin/bash

./build.sh dev
while inotifywait -r -e modify -e move -e create -e delete js/ ; do
    clear
    ./build.sh dev
done
