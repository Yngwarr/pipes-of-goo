#!/bin/bash

echo $#

if [[ $# -lt 1 ]] ; then
    echo -e "\e[1mUsage:\e[0m ./json.sh sheet.png" >&2
    echo "Creates a json spritesheet from Kenney's XMLs." >&2
    exit 1
fi

SHEET="$1"

sed -e 's/<TextureAtlas imagePath="sheet.png">/{ "meta": { "image": "'"$SHEET"'" }, "frames": {/'\
    -e 's/<SubTexture name=\(".\+"\) x="\(.\+\)" y="\(.\+\)" width="\(.\+\)" height="\(.\+\)"\/>/\1: { "frame": { "x": \2, "y": \3, "w": \4, "h": \5 } },/'\
    -e 's/<\/TextureAtlas>/} }/' "$(sed 's/.png/.xml/' <<< "$SHEET")"
