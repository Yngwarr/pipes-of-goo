BEGIN {
    print_it = 1;
    minimized = "        <script src='app.js'></script>"
}

/<!--SCRIPTS-->/ {
    print_it = 0;
    print minimized;
}

print_it == 1 { print; }

/<!--\/SCRIPTS-->/ { print_it = 1; }

